#ifndef SPTLOG_H
#define SPTLOG_H

namespace spt
{
	class DbgLog
	{
	public:
		DbgLog& operator<<(const char*Msg);
		DbgLog& operator<<(int64 data);
		DbgLog& Print(const char* Msg);
		DbgLog& Stamp();
	};


	extern DbgLog LogReal;
	extern DbgLog LogMsg;
	extern DbgLog LogWarn;
	extern DbgLog LogErr;
}

#endif // !SPTLOG_H