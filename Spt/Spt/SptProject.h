#ifndef SPTPROJECT_H
#define SPTPROJECT_H

#include"SptTypes.h"
#include"SptLock.h"
#include"SptAdt.h"
#include"SptDate.h"
#include"SptString.h"
#include"SptLog.h"
#include"SptTask.h"
#include"SptDateTask.h"
#include"SptAngSamp.h"
#include"SptResource.h"


#endif // !SPTPROJECT_H