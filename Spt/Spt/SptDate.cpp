#include "SptProject.h"
using namespace spt;



void spt::SptDate::Stamp()
{
	TimeStamp stamp;

	tTimeTask.Stamp(stamp);
	Stamp(stamp);

}

void spt::SptDate::Stamp(const TimeStamp& Stamp)
{
	t_time ns = Stamp.Ns();
	ns = ns / 1000;

	us = ns%1000;
	ns = ns / 1000;

	ms = ns % 1000;
	ns = ns / 1000;

	sec = ns % 60;
	ns = ns / 60;

	minute = ns % 60;
	ns = ns / 60;

	uint32 h = (uint32)ns;

	if (Stamp.Q().st.leap)
	{
		if (Stamp.Q().st.posleap)
		{
			if ((minute==0)&&(sec==0))
			{
				h--;
				minute = 59;
				sec = 60;
			}
		}
		else if (Stamp.Q().st.negleap)
		{
			if ((minute == 59) && (sec == 59))
			{
				h++;
				minute = 0;
				sec = 0;
			}
		}
	}
	//fixme 时区处理
	h = h + 8;

	hour = h % 24;
	h = h / 24;

	uint16 y = 1970;
	
	if (h > 17897)
	{
		y = 2019;
		h -= 17897;
	}


	

	//uint32 dd = 0;
	uint16 yearday = YearDay(y);
	while (h >= yearday)
	{
		h -= yearday;
		y++;
		//dd += yearday;
		//LogMsg << y << " " << dd<<"\n";
		yearday = YearDay(y);
	}

	year = y;

	const uint16 monday[2][12] =
	{
		{31,28,31,30,31,30,31,31,30,31,30,31},
		{31,29,31,30,31,30,31,31,30,31,30,31}
	};

	bool8 leap = IsLeapYear(y);

	uint8 mt = 0;

	while (h >= monday[leap][mt])
	{
		h -= monday[leap][mt];
		mt++;
	}

	month = mt + 1;
	mday = h + 1;
}

bool8 spt::SptDate::IsLeapYear(uint16 year)
{
	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
	{
		return 1;
	}

	return bool8(0);
}

uint16 spt::SptDate::YearDay(uint16 year)
{
	if (IsLeapYear(year))
	{
		return 366;
	}
	return uint16(365);
}

const char* spt::SptDate::toStrFmt1(StringBase& Str)
{
	Str.Append(year);
	Str.Append("年");
	Str.Append(month, 2, '0');
	Str.Append("月");
	Str.Append(mday, 2, '0');
	Str.Append("日");
	Str.Append(hour, 2, '0');
	Str.Append(":");
	Str.Append(minute, 2, '0');
	Str.Append(":");
	Str.Append(sec, 2, '0');

	return Str;
}

const char* spt::SptDate::toStrFmt2(StringBase& Str)
{
	toStrFmt1(Str);
	Str.Append(".");
	Str.Append(ms, 3, '0');
	return Str;
}

const char* spt::SptDate::toStrFmt3(StringBase& Str)
{
	toStrFmt2(Str);
	Str.Append(".");
	Str.Append(us, 3, '0');
	return Str;
}