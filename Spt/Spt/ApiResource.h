#ifndef APIRESOURCE_H
#define APIRESOURCE_H

namespace api
{
	using spt::t_unitype;

	template <const unsigned ItemNum>
	class UnitypeArray:public spt::UnitypeArrayBase
	{
	public:
		UnitypeArray():spt::UnitypeArrayBase(arr, ItemNum){}
	private:
		t_unitype arr[ItemNum];
	};

	template <const unsigned ItemNum,const unsigned FrameNum>
	class RtIoResource :public spt::RtIoResourceBase
	{
	public:
		RtIoResource() :spt::RtIoResourceBase(basearr, FrameNum) {
			for (uint32 i = 0; i < FrameNum; i++)
			{
				basearr[i] = arr + i;
			}
		}
	private:
		spt::UnitypeArrayBase* basearr[FrameNum];
		UnitypeArray<ItemNum> arr[FrameNum];
	};




}

#endif // !APIRESOURCE_H