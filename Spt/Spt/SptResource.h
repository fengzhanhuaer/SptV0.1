#ifndef SPTRESOURCE_H
#define SPTRESOURCE_H

namespace spt
{
	typedef struct
	{
		uint32 bit0 : 1;
		uint32 bit1 : 1;
		uint32 bit2 : 1;
		uint32 bit3 : 1;
		uint32 bit4 : 1;
		uint32 bit5 : 1;
		uint32 bit6 : 1;
		uint32 bit7 : 1;
		uint32 bit8 : 1;
		uint32 bit9 : 1;
		uint32 bit10 : 1;
		uint32 bit11 : 1;
		uint32 bit12 : 1;
		uint32 bit13 : 1;
		uint32 bit14 : 1;
		uint32 bit15 : 1;
		uint32 bit16 : 1;
		uint32 bit17 : 1;
		uint32 bit18 : 1;
		uint32 bit19 : 1;
		uint32 bit20 : 1;
		uint32 bit21 : 1;
		uint32 bit22 : 1;
		uint32 bit23 : 1;
		uint32 bit24 : 1;
		uint32 bit25 : 1;
		uint32 bit26 : 1;
		uint32 bit27 : 1;
		uint32 bit28 : 1;
		uint32 bit29 : 1;
		uint32 bit30 : 1;
		uint32 bit31 : 1;
	}t_bit32;
	typedef union
	{
		uint32 u32;
		int32 i32;
		uint16 u16;
		int16 i16;
		uint8 u8;
		int8 i8;
		bool8 b8;
		bool16 b16;
		bool32 b32;
		float32 f32;
		t_bit32 bit32;
	}t_unitype;

	class UnitypeArrayBase :public ArrayBase<t_unitype>
	{
	public:
		UnitypeArrayBase(t_unitype* Arr, unsigned Len) :ArrayBase<t_unitype>::ArrayBase(Arr,Len) {}
	};
	
	class RtIoResourceBase:public RingBase<UnitypeArrayBase*const>
	{
	public:
		RtIoResourceBase(UnitypeArrayBase**Base,uint32 Len) :RingBase<UnitypeArrayBase*const>::RingBase(Base,Len)
		{

		}
	private:

	};

}

#endif // !SPTRESOURCE_H