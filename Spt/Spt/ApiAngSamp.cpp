#include "ApiProject.h"
using namespace api;

spt::AngSampItem* api::AngSampDes::Samp(void* Para)
{
	ApiMain* api = (ApiMain*)Para;



	return & ((*(api->InstAppInt().CurAngSampArray()))[innerNo]);
}

uint32 api::AngSampDes::FrameNo(void* Para)
{
	ApiMain* api = (ApiMain*)Para;

	return uint32(api->InstAppInt().CurAngSampArray()->FrameNum());
}
