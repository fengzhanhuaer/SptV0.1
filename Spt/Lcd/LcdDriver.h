#ifndef  LCDDRIVER_H
#define LCDDRIVER_H


const uint32 CN_LCD_WIDTH_BYTE = CN_LCD_WIDTH >> 3;

class LcdDriver
{
public:
	void DrawLine(uint16 x1, uint16 y1, uint16 x2, uint16 y2, uint16 width);
	void DrawRect(uint16 x1, uint16 y1, uint16 x2, uint16 y2, uint16 width);
	void DrawText(uint16 x, uint16 y,const uint8* text);
	void DrawRotText(uint16 x,uint16 y,uint16 dispLen,const uint8*text);
public:
	void doLcdDraw();
	void doLcdLeftRotation();
protected:
	void mixFont();
private:
	uint8 dispbuf[CN_LCD_HEITH][CN_LCD_WIDTH_BYTE];
	uint8 fontbuf[CN_LCD_HEITH][CN_LCD_WIDTH_BYTE];
	uint8 rvsbuf[CN_LCD_HEITH][CN_LCD_WIDTH_BYTE];
	uint8 picbuf[CN_LCD_HEITH][CN_LCD_WIDTH_BYTE];

	typedef struct 
	{
		bool8 dispenable;
		bool8 rotenable;
		uint32 dstx;
		uint32 dsty;
		uint32 dispLen;
		uint32 strLen;
		uint32 deltax;
		uint32 deltay;
		uint8 buf[CN_FONT_HEIGHT][CN_LCD_WIDTH_BYTE*2];
	}rotline;

	rotline rotationbuf[CN_LCD_HEITH / CN_FONT_HEIGHT];
};

extern LcdDriver lcdDriver;


#endif // ! LCDDRIVER_H
