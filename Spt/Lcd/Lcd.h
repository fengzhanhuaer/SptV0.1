#ifndef  LCD_H
#define LCD_H


const uint32 CN_LCD_WIDTH = 240;
const uint32 CN_LCD_HEITH = 160;


void lcd_write_buf(uint32 x,uint32 y,uint8 data);
uint8 lcd_read_buf(uint32 x, uint32 y);

void lcd_buf_disp();
void hal_lcd_ini();


#endif // ! LCD_H
