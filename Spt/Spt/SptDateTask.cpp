#include"SptProject.h"
using namespace spt;

#include <time.h>

namespace spt
{
	TimeTask tTimeTask;
}

int32 spt::TimeTask::LoopOnce(void* Para)
{
	UpdateGlobalStamp();
	return int32(0);
}
int32 spt::TimeTask::PowerUp(void* Para)
{
	GlobalStampIni();
	powerUpMs = 0;
	return int32(0);
}
spt::TimeTask::TimeTask()
{
	msCnt = 0;
	powerUpMs = 0;
	globalStamp.ns = 0;
	globalStamp.q.u32 = 0;
}
void spt::TimeTask::UpdateGlobalStamp()
{
	t_time ns = globalStamp.ns;

	ns += 1000000;
	globalStamp.ns = ns;

	powerUpMs++;
	msCnt++;
}

void spt::TimeTask::GlobalStampIni()
{
	t_time ns = time(0) * 1000000000;

	globalStamp.ns = ns;

}
void spt::TimeTask::Stamp(TimeStamp& Stamp)
{
	Stamp.Ns(globalStamp.ns);
	Stamp.q.u32 = globalStamp.q.u32;

	if (Stamp.ns != globalStamp.ns)
	{
		Stamp.Ns(globalStamp.ns);
		Stamp.q.u32 = globalStamp.q.u32;
	}
}
void spt::TimeTask::Stamp(SptDate& Stamp)
{
	TimeStamp stamp;

	this->Stamp(stamp);
	Stamp.Stamp(stamp);
}

uint64 spt::TimeTask::PowerUpMs()
{
	uint64 data;

	data = powerUpMs;

	if (data != powerUpMs)
	{
		data = powerUpMs;
	}
	return uint64(data);
}

uint32 spt::TimeTask::MsCnt()
{
	uint32 data;

	data = msCnt;

	if (data != msCnt)
	{
		data = msCnt;
	}
	return uint32(data);
}
