#ifndef SPTANGSAMP_H
#define SPTANGSAMP_H

namespace spt
{
	typedef union 
	{
		struct
		{
			uint32 invalid:1;
		}st;
		uint32 u32;
	}AngSampItemQ;
	typedef struct
	{
		float32 v;
		AngSampItemQ Q;
	}AngSampItem;

	class AngSampDes
	{
	public:
	protected:
		uint32 innerNo;
		uint32 outerNo;
	private:
		
	};

	class AngSampDesArrayBase :public ArrayBase<AngSampDes>
	{
	public:
		AngSampDesArrayBase(AngSampDes*Des,uint32 Len) :ArrayBase<AngSampDes>::ArrayBase(Des,Len){}
	private:

	};
	class AngSampItemArrayBase :public ArrayBase<AngSampItem>
	{
	public:
		AngSampItemArrayBase(AngSampItem* Des, uint32 Len);
	public:
		bool8 FrameOk() { return frameOk; }
		bool8 FrameOk(bool8 Ok) { return frameOk=Ok; }

		uint32 FrameNum() { return frameNum; }
		uint32 FrameNum(uint32 Num) { return frameNum = Num; }

		t_time FrameT() { return framet; }
		t_time FrameT(t_time T) { return framet = T; }
	private:
		bool8 frameOk;
		uint32 frameNum;
		t_time framet;
	};
	class AngSampItemRingBase :public RingBase<AngSampItemArrayBase*const>
	{
	public:
		AngSampItemRingBase(AngSampItemArrayBase**Des, uint32 Len) :RingBase<AngSampItemArrayBase*const>::RingBase(Des, Len) {}
	private:

	};

	class AngSampResource :public spt::AngSampItemRingBase, public spt::AngSampDesArrayBase
	{
	public:
		AngSampResource(AngSampItemArrayBase** Arr, uint32 Len, AngSampDes* Des, uint32 Num) :
			AngSampItemRingBase(Arr,Len), AngSampDesArrayBase(Des,Num)
		{
		}
	};


	class AngSampProcess:public Task
	{
	public:
		virtual int32 LoopOnce(void* Para);
		virtual int32 PowerUp(void* Para);
	public:
		void SampProcess();
		void RgsResource(AngSampResource* Resource) { resource = Resource; }
	public:
		
	private:
		AngSampResource* resource;
		uint32 frameNum;
	};

}

#endif // !SPTANGSAMP_H