#include "ApiProject.h"
using namespace spt;
using namespace api;

ApiMain api::ApiApp;

void ApiPowerUp()
{
	LogMsg.Stamp() << " Program run into ApiPowerUp.\n";
	tTimeTask.GlobalStampIni();
}



int32 api::ApiMain::PowerUp()
{
	AddIntTask(&tTimeTask);
	AddIntTask(&AngSamp);
	appInt.Para(this);
	AddIntTask(&appInt);//Ӧ���ж�

	appResource->AppPowerUp(0);

	IntTask.PowerUp(0);
	RoundTask.PowerUp(0);
	PriTask.PowerUp(0);
	return int32(0);
}

int32 api::ApiMain::Process()
{
	IntTask.Run(0);
	RoundTask.Run(0);
	PriTask.Run(0);
	return int32(0);
}

void api::ApiMain::RgsResource(AppResource* Resource)
{
	appResource = Resource;
	if (!appResource)
	{
		return ;
	}
	AngSamp.RgsResource(appResource->angSampResource);
	appInt.CallBack(appResource->AppSampInt);

}

int32 ApiAppRgsPowerUp();

int main()
{
	LogMsg.Stamp() << " Program run into main.\n";
	ApiPowerUp();
	ApiAppRgsPowerUp();
	ApiApp.PowerUp();

	while (1)
	{
		ApiApp.Process();
	}
	return 0;
}

bool8 api::AppRoundTask::Stop()
{
	return bool8(1);
}

bool8 api::AppPriTask::Stop()
{
	return bool8(1);
}

