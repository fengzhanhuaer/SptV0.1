#ifndef APIANGSAMP_H
#define APIANGSAMP_H


namespace api
{
	class AngSampDes :public spt::AngSampDes
	{
	public:
	public:
		spt::AngSampItem* Samp(void*Para);
		uint32 FrameNo(void* Para);
	private:

	};

	template <const unsigned ItemNum>
	class AngSampItemArray :public spt::AngSampItemArrayBase
	{
	public:
		AngSampItemArray() :spt::AngSampItemArrayBase(arr, ItemNum) {}
	private:
		spt::AngSampItem arr[ItemNum];
	};

	template <const unsigned ItemNum, const unsigned FrameNum>
	class AngSampItemRing :public spt::AngSampResource
	{
	public:
		AngSampItemRing(AngSampDes*Arr):AngSampResource(sparr,FrameNum,Arr, ItemNum)
		{
			for (uint32 i = 0; i < FrameNum; i++)
			{
				sparr[i] = samp + i;
			}
		}
	private:
		AngSampItemArray<ItemNum> samp[FrameNum];
		spt::AngSampItemArrayBase* sparr[FrameNum];


	};
	

}
#endif // !APIANGSAMP_H
