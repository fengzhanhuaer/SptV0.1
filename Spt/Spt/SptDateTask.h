#ifndef SPTDATETASK_H
#define SPTDATETASK_H

namespace spt
{
	class TimeTask:public Task
	{
	public:
		TimeTask();
	public:
		void UpdateGlobalStamp();
		void GlobalStampIni();
	public:
		virtual int32 LoopOnce(void* Para);
		virtual int32 PowerUp(void* Para);
	//interface
	public:
		void Stamp(TimeStamp& Stamp);
		void Stamp(SptDate& Stamp);
		uint64 PowerUpMs();
		uint32 MsCnt();
	private:
		volatile TimeStamp globalStamp;
		volatile uint64 powerUpMs;
		volatile uint32 msCnt;
	};


	extern TimeTask tTimeTask;




}

#endif // !SPTDATETASK_H