#ifndef APIAPPINT_H
#define APIAPPINT_H

namespace api
{
	class AppInt :public spt::Task
	{
	public:
		virtual int32 LoopOnce(void* Para);
		spt::AngSampItemArrayBase* CurAngSampArray() { return pAngSampArray; };
	private:
		spt::AngSampItemArrayBase* pAngSampArray;
	};



}

#endif // !APIAPPINT_H
