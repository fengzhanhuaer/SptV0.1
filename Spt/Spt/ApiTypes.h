#ifndef APITYPES_H
#define APITYPES_H

namespace api
{
	using spt::uint8;
	using spt::uint16;
	using spt::uint32;
	using spt::uint64;

	using spt::int8;
	using spt::int16;
	using spt::int32;
	using spt::int64;

	using spt::bool8;
	using spt::bool16;
	using spt::bool32;

	using spt::float32;

	typedef spt::SptCallBack AppCallBack;

}

#endif // !APITYPES_H
