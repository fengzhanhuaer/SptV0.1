#include "ApiProject.h"
using namespace spt;
using namespace api;



int32 api::AppInt::LoopOnce(void* Para)
{
	ApiMain *api = (ApiMain*)Para;

	spt::AngSampResource* sampRes = api->Resource()->angSampResource;
	AngSampItemArrayBase* sampArr = sampRes->BottomBuf();
	pAngSampArray = 0;
	if (sampArr)
	{
		if (sampArr->FrameOk())
		{
			pAngSampArray = sampArr;
		}
	}
	


	Task::LoopOnce(Para);

	if(pAngSampArray)
	{
		sampRes->Pop();
		sampArr->FrameOk(0);
	}

	return int32(0);
}
