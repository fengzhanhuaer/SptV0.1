#ifndef APILOG_H
#define APILOG_H

namespace api
{
	using spt::LogReal;
	using spt::LogMsg;
	using spt::LogWarn;
	using spt::LogErr;
}

#endif // !APILOG_H