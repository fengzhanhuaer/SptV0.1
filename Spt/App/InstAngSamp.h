#ifndef INSTANGSAMP_H
#define INSTANGSAMP_H

typedef enum 
{
	AS_DIR_Start = 0,
	AS_Ua = AS_DIR_Start,
	AS_Ub,
	AS_Uc,
	AS_DIR_End
}E_AngSamp;


typedef AngSampItemRing<AS_DIR_End, 24 * 2> InstAngSampItemRing;



extern AngSampDes gInstAngSampDes[AS_DIR_End];
extern InstAngSampItemRing gAngSampItemRing;

#endif // !INSTANGSAMP_H
