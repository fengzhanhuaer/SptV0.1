#include "LcdProject.h"
#include"stdio.h"
#include "windows.h"

typedef struct stLcd_buf
{
    uint8 buf[CN_LCD_HEITH+10][CN_LCD_WIDTH_BYTE+2];
    HWND hwnd = 0;
    HDC hdc = 0;
    HBITMAP hBitmap = 0;
    HDC  hdcMem = 0;
    BITMAP bitmap = { 0,  CN_LCD_WIDTH + 16,  CN_LCD_HEITH + 10,  (CN_LCD_WIDTH + 16) / 8 , 1, 1 };
}LCD_buf;

LCD_buf lcd_buf;

void lcd_write_buf(uint32 x, uint32 y, uint8 data)
{
    if ( x >= CN_LCD_WIDTH_BYTE)
    {
        printf("x overflow.\n");
        return;
    }
    if (y >= CN_LCD_HEITH)
    {
        printf("x overflow.\n");
        return;
    }

    lcd_buf.buf[y+5][x+1] = ~data;
}

uint8 lcd_read_buf(uint32 x, uint32 y)
{
    if (x >= CN_LCD_WIDTH_BYTE)
    {
        printf("x overflow.\n");
        return 0;
    }
    if (y >= CN_LCD_HEITH)
    {
        printf("y overflow.\n");
        return 0;
    }
    return uint8(~lcd_buf.buf[y+5][x+1]);
}

void lcd_buf_disp()
{
    uint32 lcdx = 20;
    uint32 lcdy = 20;

    lcd_buf.hdc = GetDC(lcd_buf.hwnd);
    if (!lcd_buf.hBitmap)
    {
        lcd_buf.hBitmap = CreateBitmapIndirect(&lcd_buf.bitmap);
    }
    SetBitmapBits(lcd_buf.hBitmap, sizeof(lcd_buf.buf), lcd_buf.buf);

    if (!lcd_buf.hdcMem)
    {
        lcd_buf.hdcMem = CreateCompatibleDC(lcd_buf.hdc);
    }
    SelectObject(lcd_buf.hdcMem, lcd_buf.hBitmap);
    int result = BitBlt(lcd_buf.hdc, lcdx, lcdy, lcd_buf.bitmap.bmWidth, lcd_buf.bitmap.bmHeight, lcd_buf.hdcMem, 0, 0, SRCCOPY);

    if (!result)
    {
        printf("void bitPicture::Paint(uint16 lcdx = 10,uint16 lcdy)  if (!result) \n");
    }
    ReleaseDC(lcd_buf.hwnd, lcd_buf.hdc);
}

void hal_lcd_ini()
{
    //获取控制台窗口句柄
    HWND console = GetConsoleWindow();

    lcd_buf.hwnd = console;
    
    for (uint16  i = 0; i < lcd_buf.bitmap.bmHeight; i++)
    {
        for (uint16 j = 0; j < lcd_buf.bitmap.bmWidthBytes; j++)
        {
            lcd_buf.buf[i][j] = 0xff;
        }
    }
}


