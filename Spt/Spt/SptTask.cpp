#include "SptProject.h"
using namespace spt;

spt::Task::Task():TaskListItem(this)
{
	para = 0;
	callback = 0;
	period = 0;
}

spt::Task::Task(const char* Name,void*Para,SptCallBack CallBack,uint32 Period):TaskListItem(this)
{
	para = 0;
	callback = CallBack;
	period = Period;
}

int32 spt::Task::LoopOnce(void* Para)
{
	if (callback)
	{
		callback(para);
	}
	return int32(0);
}

int32 spt::Task::PowerUp(void* Para)
{
	return int32(0);
}

spt::IntTaskDispt::IntTaskDispt():SList<Task>::SList()
{
	
}

int32 spt::IntTaskDispt::Run(void* Para)
{
	Task* now;
	if (!root)
	{
		return 0;
	}
	now = root->Item();
	while (now)
	{
		now->LoopOnce(now->para);
		if (now->Next())
		{
			now = now->Next()->Item();
		}
		else
		{
			break;
		}
	}
	return int32(0);
}

int32 spt::IntTaskDispt::PowerUp(void* Para)
{
	Task* now;
	if (!root)
	{
		return 0;
	}
	now = root->Item();
	while (now)
	{
		now->PowerUp(now->para);
		if (now->Next())
		{
			now =  now->Next()->Item();
		}
		else
		{
			break;
		}
	}
	return int32(0);
}


spt::RoundTaskDispt::RoundTaskDispt() :RSList<Task>::RSList()
{
	now = 0;
}

bool8 spt::RoundTaskDispt::Stop()
{
	return bool8(1);
}

int32 spt::RoundTaskDispt::Run(void* Para)
{
	if (!root)
	{
		return 0;
	}
	if (!now)
	{
		now = root->Item();
	}
	while (now)
	{
		now->LoopOnce(now->para);
		if (now->Next())
		{
			now = now->Next()->Item();
		}
		else
		{
			break;
		}
		if (Stop())
		{
			return 0;
		}
	}
	return int32(0);
}

int32 spt::RoundTaskDispt::PowerUp(void* Para)
{
	if (!root)
	{
		return 0;
	}
	Task* now;
	now = root->Item();
	do
	{
		now->PowerUp(now->para);
		if (now->Next())
		{
			now = now->Next()->Item();
		}
		else
		{
			break;
		}
	} while (now && (now != root));
	return int32(0);
}

spt::PriTaskDispt::PriTaskDispt() :RSList<Task>::RSList()
{
	now = 0;
}

bool8 spt::PriTaskDispt::Stop()
{
	return bool8(1);
}

int32 spt::PriTaskDispt::Run(void* Para)
{
	if (!root)
	{
		return 0;
	}
	if (!now)
	{
		now = root->Item();
	}
	while (now)
	{
		now->LoopOnce(now->para);
		if (now->Next())
		{
			now = now->Next()->Item();
		}
		else
		{
			break;
		}
		if (Stop())
		{
			return 0;
		}
	}
	return int32(0);
}

int32 spt::PriTaskDispt::PowerUp(void* Para)
{
	if (!root)
	{
		return 0;
	}
	Task* now;
	now = root->Item();
	do
	{
		now->PowerUp(now->para);
		if (now->Next())
		{
			now = now->Next()->Item();
		}
		else
		{
			break;
		}
	} while (now && (now != root));
	return int32(0);
}