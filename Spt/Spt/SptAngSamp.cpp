#include "SptProject.h"
using namespace spt;


int32 spt::AngSampProcess::LoopOnce(void* Para)
{
	SampProcess();


	return int32(0);
}

int32 spt::AngSampProcess::PowerUp(void* Para)
{
	return int32();
}

void spt::AngSampProcess::SampProcess()
{
	if (!resource)
	{
		return;
	}
	AngSampItemRingBase* ring = resource;
	AngSampDesArrayBase* desArr = resource;

	if ((!ring) || (!desArr))
	{
		return ;
	}
	AngSampItemArrayBase* itemarr = ring->TopBuf();
	if (!itemarr)
	{
		return;
	}

	
	

	itemarr->FrameOk(1);
	itemarr->FrameNum(frameNum++);

	ring->Push();
}

spt::AngSampItemArrayBase::AngSampItemArrayBase(AngSampItem* Des, uint32 Len) :ArrayBase<AngSampItem>::ArrayBase(Des, Len)
{
	frameOk = 0;
	frameNum = 0;
	framet = 0;
}
