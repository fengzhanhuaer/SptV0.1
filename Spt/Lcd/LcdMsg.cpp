#include "LcdProject.h"



LcdMsgBufBase::LcdMsgBufBase(uint8* Buf, uint32 BufLen):buf(Buf),bufLen(BufLen)
{
	reader = writer = 0;
}

void LcdMsgBufBase::WriteBuf(uint8* msg, uint16 MsgLen)
{
	if (MsgLen >= bufLen)
	{
		return;
	}

	if (!bufLen)
	{
		return;
	}

	if (reader < writer)
	{
		uint32 len = reader + bufLen - writer;

		if (len < MsgLen)
		{
			return;
		}
	}
	else if(reader > writer)
	{
		uint32 len = reader - writer;
		if (len < MsgLen)
		{
			return;
		}
	}


	uint32 w = writer % bufLen;

	while ((w != reader)&&(MsgLen--))
	{
		buf[w] = *msg++;

		w = (w +1) % bufLen;
	}

	writer = w;

}

bool8 LcdMsgBufBase::HasNewData()
{
	return bool8(reader!=writer);
}

uint8 LcdMsgBufBase::Pop()
{
	uint8 data;
	if (HasNewData())
	{
		data = buf[reader];
		reader = (reader + 1) % bufLen;
		return data;
	}

	return uint8(0);
}
