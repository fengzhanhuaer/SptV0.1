#ifndef  TYPE_H
#define TYPE_H


typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef unsigned long long uint64;

typedef uint8 bool8;
typedef uint16 bool16;
typedef uint32 bool32;

typedef signed char int8;
typedef signed short int16;
typedef signed int int32;
typedef signed long long int64;

#define M_ItemNum(arr)	((uint32)(sizeof(arr))/sizeof(arr[0]))



#endif // ! TYPE_H