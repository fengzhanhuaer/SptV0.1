#ifndef LCDMSG_H
#define LCDMSG_H


class LcdMsgBufBase
{
public:
    LcdMsgBufBase(uint8*Buf,uint32 BufLen);
public:
    void WriteBuf(uint8* msg, uint16 MsgLen);
    bool8 HasNewData();
    uint8 Pop();
protected:
    uint8* buf;
    uint32 bufLen;
    uint32 writer;
    uint32 reader;
};


template <const uint32 BufLen>
class LcdMsgBuf :public LcdMsgBufBase
{
public:
    LcdMsgBuf() :LcdMsgBufBase(u8buf, BufLen) {}
private:
    uint8 u8buf[BufLen];

};


#endif 
