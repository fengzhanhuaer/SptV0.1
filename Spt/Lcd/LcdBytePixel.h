#ifndef SPTBYTEPIXEL_H
#define SPTBYTEPIXEL_H


const unsigned int CN_FONT_HEIGHT = 16;
const unsigned int CN_FONT_WIDTH = 8;

extern unsigned char gASSIC16[][CN_FONT_HEIGHT];
extern const unsigned int gASSIC16Len;
extern unsigned char g_HZK16[][CN_FONT_HEIGHT *2];
extern const unsigned int  g_HZK16Len;


#endif // SPTBYTEPIXEL_H
