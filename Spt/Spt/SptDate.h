#ifndef SPTDATE_H
#define SPTDATE_H

namespace spt
{
	typedef int64 t_time;

	class TimeStamp
	{
	public:
		typedef union
		{
			struct
			{
				uint32 leap : 1;
				uint32 negleap : 1;
				uint32 posleap : 1;
			}st;
			uint32 u32;
		}q_time;
	public:
		t_time Ns() const{ return ns; }
		void Ns(t_time ins) {ns = ins; }
		q_time Q()const{ return q; }
		void Q(q_time timeq){ q.u32 = timeq.u32; }
	public:
		t_time ns;//单位ns
		q_time q;
	};
	class SptDate
	{
	public:
		void Stamp();
		void Stamp(const TimeStamp&Stamp);
	public:
		bool8 IsLeapYear(uint16 year);
		uint16 YearDay(uint16 year);
	public:
		const char* toStrFmt1(class StringBase& Str);//1970年01月01日01:01:01
		const char* toStrFmt2(class StringBase& Str);//1970年01月01日01:01:01.000
		const char* toStrFmt3(class StringBase& Str);//1970年01月01日01:01:01.000.000
	public:
		uint16 Ms() { return ms; };
		uint16 Us() { return us; }
		uint16 Year() { return year; }
		uint8 Month() { return month; }
		uint8 MDay() { return mday; }
		uint8 Hour() { return hour; }
		uint8 Minute() { return minute; }
		uint8 Second() { return sec; }
	private:
		uint16 ms;
		uint16 us;
		uint16 year;
		uint8 month;
		uint8 mday;
		uint8 hour;
		uint8 minute;
		uint8 sec;
	};

	

}

#endif // !SPTDATE_H
