#include<stdio.h>
#include "SptProject.h"
using namespace spt;

namespace spt
{
	DbgLog LogReal;
	DbgLog LogMsg;
	DbgLog LogWarn;
	DbgLog LogErr;
}

DbgLog& spt::DbgLog::operator<<(const char* Msg)
{
	return Print(Msg);
}

DbgLog& spt::DbgLog::operator<<(int64 data)
{
	String40 str;
	str.Append(data);

	(*this) << str;

	return *this;
}

DbgLog& spt::DbgLog::Print(const char* Msg)
{
	printf(Msg);

	return *this;
}

DbgLog& spt::DbgLog::Stamp()
{
	String40 str;

	SptDate date;

	date.Stamp();

	(*this)<<date.toStrFmt3(str);

	return *this;
}
