#ifndef APIMAIN_H
#define APIMAIN_H

namespace api
{
	class AppRoundTask :public spt::RoundTaskDispt
	{
	public:
		virtual bool8 Stop();
	private:

	};
	class AppPriTask :public spt::PriTaskDispt
	{
	public:
		virtual bool8 Stop();
	private:

	};
	

	typedef struct
	{
		AppCallBack AppPowerUp;
		AppCallBack AppSampInt;
		spt::AngSampResource *angSampResource;
	}AppResource;


	class ApiMain
	{
	public:
		int32 PowerUp();
		int32 Process();
	public:
		void AddIntTask(spt::Task* task) { IntTask.AddItem(task);};
		void AddRoundTask(spt::Task* task) { RoundTask.AddItem(task); };
		void AddPriTask(spt::Task* task) { PriTask.AddItem(task); };
	public:
		void RgsResource(AppResource* Resource);
		AppResource* Resource() { return appResource; };
		AppInt& InstAppInt() { return appInt; };
		spt::AngSampProcess& InstAngSampProcess() { return AngSamp; };
	private:
		spt::AngSampProcess AngSamp;
		spt::IntTaskDispt IntTask;
		AppRoundTask RoundTask;
		AppPriTask PriTask;
		AppResource* appResource;
		AppInt appInt;
	};

	extern ApiMain ApiApp;
}

#endif // !APIMAIN_H