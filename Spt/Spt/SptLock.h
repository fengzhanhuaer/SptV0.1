#ifndef SPTLOCK_H
#define SPTLOCK_H

namespace spt
{
	class WRLock
	{
	public:
		bool8 WLock();
		bool8 RLock();
		bool8 RDirty();
		void WUnLock();
		void RUnLock();
		void UnLock();
	private:
		volatile bool8 is_rdirty;
		volatile bool8 is_write;
		volatile uint8 write_cnt;
		volatile uint8 read_cnt;
	};

	class AutoLock
	{
	public:
		AutoLock(WRLock& Lock);
		~AutoLock();
	public:
		bool8 RDirty();
		bool8 WLock();
		void WUnLock();
		bool8 RLock();
		void RUnLock();
	private:
		WRLock& lock;
		bool8 is_write;
		bool8 is_read;
	};

}

#endif // !SPTLOCK_H
