#include "SptProject.h"
using namespace spt;

const char num2assic[] = "0123456789ABCDEF";

StringBase& spt::StringBase::Append(const char* Msg)
{
	const char* sur = Msg;

	while ((*sur) && (Push(*sur)))
	{
		sur++;
	}

	if (top<ArrLen())
	{
		arr[top] = 0;
	}
	
	return *this;
}

StringBase& spt::StringBase::Append(int64 Data)
{
	String40 str;
	int64 data = Data;
	bool8 negflag=0;
	if (data < 0)
	{
		negflag = 1;
		data = -data;
	}
	char onedata;
	do
	{
		onedata = data % 10;
		data = data / 10;
		str.Push(num2assic[onedata]);
	} while (data);
	if (negflag)
	{
		str.Push('-');
	}

	while (str.PushBack(onedata))
	{
		Push(onedata);
	}

	return *this;
}

StringBase& spt::StringBase::Append(int64 Data, uint8 Len, char fill)
{
	String40 str;

	str.Append(Data);
	char data;

	uint8 strlen = str.StrLen();

	uint8 filllen = 0;

	if (strlen < Len)
	{
		filllen = Len - strlen;
		Len -= filllen;
	}

	while (filllen)
	{
		filllen--;
		Push(fill);
	}

	while (Len && str.Pop(data))
	{
		Len--;
		Push(data);
	}
	return *this;
}

StringBase& spt::StringBase::Append(int64 Data, uint8 Len, uint8 fixbit, char fill)
{
	const uint64 coe[] = { 10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000,100000000000 };
	String40 fixstr;
	int64 data = Data;
	bool8 negflag = 0;

	if (data < 0)
	{
		negflag = 1;
		data = -data;
	}

	if (fixbit)
	{
		int64 fixdata = data % coe[fixbit - 1];
		data = data/coe[fixbit-1];
		fixstr.Append(".");
		fixstr.Append(fixdata,fixbit,'0');
	}
	String40 str;

	char onedata;
	do
	{
		onedata = data % 10;
		data = data / 10;
		str.Push(num2assic[data]);
	} while (data);
	if (negflag)
	{
		str.Push('-');
	}

	uint32 strlen = str.StrLen() + fixstr.StrLen();

	uint8 filllen = 0;

	if (strlen < Len)
	{
		filllen = Len - strlen;
		Len -= filllen;
	}

	while (filllen)
	{
		filllen--;
		Push(fill);
	}

	while (Len&&str.PushBack(onedata))
	{
		Len--;
		Push(onedata);
	}
	while (Len && fixstr.PushBack(onedata))
	{
		Len--;
		Push(onedata);
	}
	return *this;
}

StringBase& spt::StringBase::Append(float32 Data, uint8 fixbit)
{
	int64 data = (int64)Data;

	Append(data);

	const float32 coe[] = { 10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000 };

	if (fixbit > 10)
	{
		fixbit = 10;
	}
	if (fixbit > 0)
	{
		fixbit--;
	}
	float32 fdata = Data - data;
	fdata = fdata * coe[fixbit];
	data = (int64)fdata;

	if (data < 0)
	{
		data = -data;
	}
	Append(data,fixbit+1,'0');

	return *this;
}
