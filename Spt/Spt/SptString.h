#ifndef SPTSTRING_H
#define SPTSTRING_H

namespace spt
{
	class StringBase:public QueueBase<char>
	{
	public:
		StringBase(char* Arr, uint32 Len):QueueBase<char>::QueueBase(Arr, Len) {}
	public:
		operator const char* () { return QueueBase<char>::arr; }
	public:
		StringBase& Append(const char* Msg);
		StringBase& Append(int64 Data);
		StringBase& Append(int64 Data,uint8 Len,char fill);
		StringBase& Append(int64 Data, uint8 Len,uint8 fixbit ,char fill);
		StringBase& Append(float32 Data,uint8 fixbit);
	public:
		uint32 StrLen() { return QueueBase<char>::top; }
	};

	template <uint32 Length>
	class String:public StringBase
	{
	public:
		String() :StringBase(array, Length) { array[0] = 0; }
	private:
		char array[Length];
	};

	typedef String<20> String20;
	typedef String<40> String40;
	typedef String<100> String100;
	typedef String<200> String200;
	typedef String<400> String400;
	typedef String<1000> String1000;

}

#endif // !SPTSTRING_H