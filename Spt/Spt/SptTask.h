#ifndef SPTTASK_H
#define SPTTASK_H

namespace spt
{
	typedef SListItem<class Task> TaskListItem;
	class Task :public TaskListItem
	{
		friend class IntTaskDispt;
		friend class RoundTaskDispt;
		friend class PriTaskDispt;
	public:
		Task();
		Task(const char* Name, void* Para, SptCallBack CallBack, uint32 Period);
	public:
		virtual int32 LoopOnce(void* Para);
		virtual int32 PowerUp(void*Para);
	public:
		void* Para() { return para; }
		void* Para(void *Pa) { return para=Pa; }
		SptCallBack CallBack() { return callback; };
		SptCallBack CallBack(SptCallBack CB) { return callback=CB; };
	private:
		void* para;
		SptCallBack callback;
		uint32 period;
	};
	class IntTaskDispt :public SList<Task>
	{
	public:
		IntTaskDispt();
	public:
		int32 Run(void*Para);
		int32 PowerUp(void*Para);
	private:

	};
	class RoundTaskDispt :public RSList<Task>
	{
	public:
		RoundTaskDispt();
	public:
		virtual bool8 Stop();
		int32 Run(void* Para);
		int32 PowerUp(void* Para);
	private:
		Task* now;
	};

	class PriTaskDispt :public RSList<Task>
	{
	public:
		PriTaskDispt();
	public:
		virtual bool8 Stop();
		int32 Run(void* Para);
		int32 PowerUp(void* Para);
	private:
		Task* now;
	};

}

#endif // !SPTTASK_H




